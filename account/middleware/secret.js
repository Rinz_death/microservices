let {ApiService,cookieParser,compression,helmet,passport } = require("../conf.js")
const { ServiceBroker } = require("moleculer");
const broker = new ServiceBroker({ 
    logger: console,
    statistics: true,
    metrics: true
});

let auth = (req,res,next)=>{
    console.log("Я тута")
}

broker.createService({
    mixins: [ApiService],
    settings: {
        // Global middlewares. Applied to all routes.
        use: [
            cookieParser(),
            helmet()
        ],
        routes: [
            {
                path: "/",

                // Route-level middlewares.
                use: [
                    compression(),
                    
                    passport.initialize(),
                    passport.session()
                ],
                
                aliases: {
                    "GET /users": [
                        // Alias-level middlewares.
                        // auth.isAuthenticated(),
                        auth(),
                        // "top.secret"
                        // auth.hasRole("admin"),
                        "users.auth" // Call the `top.secret` action
                    ]
                }
            }
        ]
    }
});