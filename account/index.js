let {ApiService,DbService,MongooseAdapter,mongoose,broker } = require("./conf.js");
// let userModel = require('./models/user.js').UserModel;
// require("./middleware/secret.js");

broker.createService({
    name: "users",
    actions: {
        create(req,res){
            console.log(req);
            return "Create user!"
        },

        auth(req,res){
            return "Ayth user!"
        },

        get(req,res){
            return "Get information!"
        },

        update(req,res){
            return "Update information!"
        },

        remove(req,res){
            return "Delete user!"
        },

    }
});



// Load API Gateway
broker.createService({
    mixins: [ApiService],

    settings: {
        mixins: [ApiService],
        
        settings: {
            routes: [{
                aliases: {
                    "POST login": "users.auth",
                    "GET users/:id": "users.get",
                    "POST reg": "users.create",
                    "PUT users/:id": "users.update",
                    "DELETE users/:id": "users.remove"
                }
            }]
        }
    }
});

// Start server
broker.start();