const { ServiceBroker } = require("moleculer");
const ApiService = require("moleculer-web");
const DbService = require("moleculer-db");
const MongooseAdapter = require("moleculer-db-adapter-mongoose");
const mongoose = require("mongoose");
const cookieParser = require('cookie-parser');
const compression = require('compression');
const helmet = require('helmet');
const passport = require('passport');

const broker = new ServiceBroker({ 
    logger: console,
    
    statistics: true,
    metrics: true 
});

module.exports.broker = broker;
module.exports.ApiService = ApiService;
module.exports.cookieParser = cookieParser;
module.exports.compression = compression;
module.exports.passport = passport;
module.exports.helmet = helmet;
module.exports.DbService = DbService;
module.exports.mongoose = mongoose;
module.exports.MongooseAdapter = MongooseAdapter;