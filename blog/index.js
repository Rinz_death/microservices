let { ServiceBroker } = require("moleculer");
let ApiService = require("moleculer-web");

let broker = new ServiceBroker({ logger: console });
const users = {
    list: [1,2]
}

broker.createService({
    name: "users",
    actions: {
        list() {
            return "Hello API Gateway!"
        }
    }
});

// Load API Gateway
broker.createService({
    mixins: [ApiService],

    settings: {
        mixins: [ApiService],
        
        settings: {
            routes: [{
                aliases: {
                    "GET users": "users.list",
                    "GET users/:id": "users.get",
                    "POST users": "users.create",
                    "PUT users/:id": "users.update",
                    "DELETE users/:id": "users.remove"
                }
            }]
        }
    }
});

// Start server
broker.start();